<link rel='stylesheet' href="http://<?=$_SERVER['HTTP_HOST']?>/assets/css/statsTableStyle.css">	
<link rel='stylesheet' href="http://<?=$_SERVER['HTTP_HOST']?>/assets/css/statsMenuStyle.css">	
<!--
<script type="text/javascript">
function showStats(){
	$.ajax({
        type: "POST",
        url:  $('#req-form').attr('action'),
        data: $('#req-form').serialize()
    }).done(function( result )
        {
           $("#statsTable").html( result );
        });
}
function setWorld(){
	document.getElementById('sf_world').value = document.getElementById(id).value;
}
function setStatType(id){
	document.getElementById('sf_type').value = document.getElementById(id).value;
}
</script>
-->
<style>
.block {
  display: none;
}
div:target {
  display: block;
}
div[id*=tab]:target {
  display: block;
}
div[id*=t]:target ~ #world {
  display: none;
}
</style>

<?
$lang = array(
	'WORLDS' => array(
		'total' => 'TOTAL',
		'DIM-42'=> 'TWILIGHT',
		'DIM-7' => 'THAUM',
		'DIM1' 	=> 'ENDER',
		'DIM-1' => 'NETHER',
		'world' => 'WORLD',
	),
	'MAIN_STATS' => array(	
		'lastleave' 	=> array('ru' => 'Последнее отключение',  	'isUsed' => '0'),
		'lastjoin' 		=> array('ru' => 'Последнее подключение', 	'isUsed' => '0'),
		'counter' 		=> array('ru' => 'Счетчик',  				'isUsed' => '0'),
		'player_id'		=> array('ru' => 'ID игрока',  				'isUsed' => '0'),
		'world' 		=> array('ru' => 'Мир',  					'isUsed' => '0'),
		'playtime' 		=> array('ru' => 'Времени в игре', 			'isUsed' => '1'),
		'arrows' 		=> array('ru' => 'Выпущено стрел',  		'isUsed' => '1'),
		'xpgained' 		=> array('ru' => 'Получено XP', 			'isUsed' => '1'),
		'joins' 		=> array('ru' => 'Подключений',  			'isUsed' => '1'),
		'fishcatched'	=> array('ru' => 'Поймано рыбы',  			'isUsed' => '1'),
		'damagetaken' 	=> array('ru' => 'Получено урона',  		'isUsed' => '1'),
		'timeskicked' 	=> array('ru' => 'Был кикнут (раз)', 		'isUsed' => '0'),
		'toolsbroken' 	=> array('ru' => 'Сломано инструментов',  	'isUsed' => '1'),
		'eggsthrown' 	=> array('ru' => 'Брошено яиц', 			'isUsed' => '1'),
		'itemscrafted' 	=> array('ru' => 'Скрафчено предметов',  	'isUsed' => '1'),
		'omnomnom' 		=> array('ru' => 'Съедено еды', 			'isUsed' => '1'),
		'onfire'		=> array('ru' => 'Был подожжен (раз)', 		'isUsed' => '0'),
		'wordssaid' 	=> array('ru' => 'Сказано слов', 			'isUsed' => '1'),
		'commandsdone' 	=> array('ru' => 'Выполнено команд',  		'isUsed' => '1'),
		'votes' 		=> array('ru' => 'Голосований',  			'isUsed' => '0'),
		'teleports' 	=> array('ru' => 'Телепортов',  			'isUsed' => '0'),
		'itempickups' 	=> array('ru' => 'Подобрано предметов', 	'isUsed' => '1'),
		'bedenter' 		=> array('ru' => 'Использовано кроватей', 	'isUsed' => '1'),
		'bucketfill' 	=> array('ru' => 'Наполнено ведер', 		'isUsed' => '1'),
		'bucketempty' 	=> array('ru' => 'Опустошено ведер', 		'isUsed' => '1'),
		'worldchange' 	=> array('ru' => 'Переходов между мирами',	'isUsed' => '1'),
		'itemdrops' 	=> array('ru' => 'Брошено предметов', 		'isUsed' => '1'),
		'shear' 		=> array('ru' => 'Подстрижено овец', 		'isUsed' => '1'),
		'pvpstreak' 	=> array('ru' => 'Комбо PVP-убийств', 		'isUsed' => '0'),
		'pvptopstreak' 	=> array('ru' => 'Максимальное PVP-комбо',	'isUsed' => '0'),
		'money' 		=> array('ru' => 'Денег', 					'isUsed' => '0'),
		'trades	' 		=> array('ru' => 'Сделок', 					'isUsed' => '0'),
	),
	'MOVE_STATS' => array(	//0 = walking, 1 = boat, 2 = minecart, 3 = pig, 4 = on a pig in a minecart, 5 = horse, 6 = flying
		'0' 		=> array('ru' => 'Пешком',  				'isUsed' => '1'),
		'1' 		=> array('ru' => 'На лодке',	 			'isUsed' => '1'),
		'2' 		=> array('ru' => 'На вагонетке',			'isUsed' => '1'),
		'3'			=> array('ru' => 'На свинье',  				'isUsed' => '1'),
		'4' 		=> array('ru' => 'На свинье в вагонетке',	'isUsed' => '0'),
		'5' 		=> array('ru' => 'На лошади',	  			'isUsed' => '1'),
		'5' 		=> array('ru' => 'В полете',	  			'isUsed' => '1'),
	),
	'DEATH_STATS' => array(
		'0' 		=> array('ru' => 'Пешком',  				'isUsed' => '1'),
		'1' 		=> array('ru' => 'На лодке',	 			'isUsed' => '1'),
		'2' 		=> array('ru' => 'На вагонетке',			'isUsed' => '1'),
		'3'			=> array('ru' => 'На свинье',  				'isUsed' => '1'),
		'4' 		=> array('ru' => 'На свинье в вагонетке',	'isUsed' => '0'),
		'5' 		=> array('ru' => 'На лошади',	  			'isUsed' => '1'),
		'5' 		=> array('ru' => 'В полете',	  			'isUsed' => '1'),
	),
	'KILL_STATS' => array(
		'0' 		=> array('ru' => 'Пешком',  				'isUsed' => '1'),
		'1' 		=> array('ru' => 'На лодке',	 			'isUsed' => '1'),
		'2' 		=> array('ru' => 'На вагонетке',			'isUsed' => '1'),
		'3'			=> array('ru' => 'На свинье',  				'isUsed' => '1'),
		'4' 		=> array('ru' => 'На свинье в вагонетке',	'isUsed' => '0'),
		'5' 		=> array('ru' => 'На лошади',	  			'isUsed' => '1'),
		'5' 		=> array('ru' => 'В полете',	  			'isUsed' => '1'),
	),
);
	
if(isset($_GET['id']))
{
	$data = $user->getUserStats($_GET['id']);
	$dataTime = $user->getUserOnlineStats($_GET['id']);
}
else
{
	$data = $user->getUserStats($_SESSION['ID']);
	$dataTime = $user->getUserOnlineStats($_SESSION['ID']);	
}
?>
<?if(isset($_GET['id'])):?>
    <h4>
		<b>Статистика игрока <?=$_GET['id'];?></b>
	</h4>
	<h5>
		<?if($dataTime['isOnline']):?>
			<span style="color:green;">Сейчас онлайн</span>
		<?else:?>
			<span style='color:rgb(150,150,150);'>
				Последний раз был в сети <?=$dataTime['lastSession']['ago'];?> назад 
				(<?=$dataTime['lastSession']['string'];?>)
			</span>
		<?endif;?>
	</h5>
<?endif;?>
<?foreach($lang["WORLDS"] as $worldID => $worldName):?>
	<a href="#<?=strtolower(str_replace(' ', '', $worldName));?>">  <?=$worldName?>  </a>
<?endforeach;?>
<!--
<form id="req-form1" name="req-form1" method="POST" action="temp.php">
</br>

<input class='stats_world_button' id='stats_w_6' type='radio' name='stat_world' value='total' checked /><label for='stats_w_6' onClick="setWorld('stats_w_6');">TOTAL</label>
<input class='stats_world_button' id='stats_w_1' type='radio' name='stat_world' value='world'	 	  /><label for='stats_w_1' onClick="setWorld('stats_w_1');">WORLD</label>
<input class='stats_world_button' id='stats_w_2' type='radio' name='stat_world' value='nether'	 	  /><label for='stats_w_2' onClick="setWorld('stats_w_2');">NETHER</label>
<input class='stats_world_button' id='stats_w_3' type='radio' name='stat_world' value='ender'	 	  /><label for='stats_w_3' onClick="setWorld('stats_w_3');">ENDER</label>
<input class='stats_world_button' id='stats_w_4' type='radio' name='stat_world' value='thaum'	 	  /><label for='stats_w_4' onClick="setWorld('stats_w_4');">THAUM</label>
<input class='stats_world_button' id='stats_w_5' type='radio' name='stat_world' value='twilight'	  /><label for='stats_w_5' onClick="setWorld('stats_w_6');">TWILIGHT</label>

</br>
<input class='stats_world_button' id='stats_t_1' type='radio' name='stat_type' value='main' checked /><label for='stats_t_1' onClick="setStatType('stats_t_1');">MAIN</label>
<input class='stats_world_button' id='stats_t_2' type='radio' name='stat_type' value='move'	 		/><label for='stats_t_2' onClick="setStatType('stats_t_2');">MOVE</label>
<input class='stats_world_button' id='stats_t_3' type='radio' name='stat_type' value='blocks'		/><label for='stats_t_3' onClick="setStatType('stats_t_3');">BLOCKS</label> 
<input class='stats_world_button' id='stats_t_4' type='radio' name='stat_type' value='kills' 		/><label for='stats_t_4' onClick="setStatType('stats_t_4');">KILLS</label>
<input class='stats_world_button' id='stats_t_5' type='radio' name='stat_type' value='deaths'		/><label for='stats_t_5' onClick="setStatType('stats_t_5');">DEATHS</label>

</form>

<form id="req-form" name="req-form" method="POST" action="temp.php">
	<input id='sf_world' name='stat_world' type='text'/>
	<input id='sf_type' name='stat_type' type='text'/>
	<a href='#' onClick="showStats();">Показать</a>
</form>
-->

<?foreach($lang["WORLDS"] as $worldID => $worldName):?>
	<div id="<?=strtolower(str_replace(' ', '', $worldName))?>" class="abs block">
		<table class="table-fill">
		<tbody class="table-hover">
			<tr>
				<td colspan="2"><center>ОБЩАЯ СТАТИСТИКА</center></td>
			</tr>
			<?foreach($lang['MAIN_STATS'] as $statName=>$statData):?>
				<?if ($statData['isUsed']):?>
					<tr>
						<td class='text-left'>
							<?=$statData['ru']?>&nbsp;&nbsp;
						</td>
						<td class='text-left'>
							<?if($statName == 'playtime'):?>
								<?=floor($data['MAIN_STATS'][$worldID][$statName]/3600)."ч ".gmdate('iмин sс', $data['MAIN_STATS'][$worldID][$statName])?>
							<?else:?>
								<?if(isset($data['MAIN_STATS'][$worldID][$statName])):?>
									<?=floor($data['MAIN_STATS'][$worldID][$statName])?>
								<?else:?>
									0
								<?endif;?>
							<?endif;?>
						</td>
					</tr>
				<?endif;?>
			<?endforeach;?>
			<tr>
				<td colspan="2"><center>СТАТИСТИКА ПЕРЕДВИЖЕНИЯ</center></td>
			</tr>
			<?foreach($lang['MOVE_STATS'] as $statName=>$statData):?>
				<?if ($statData['isUsed']):?>
					<tr>
						<td class='text-left'>
							<?=$statData['ru']?>&nbsp;&nbsp;
						</td>
						<td class='text-left'>
							<?if(isset($data['MOVE_STATS'][$worldID][$statName])):?>
								<?=floor($data['MOVE_STATS'][$worldID][$statName])?>
							<?else:?>
								0
							<?endif;?>
						</td>
					</tr>	
				<?endif;?>
			<?endforeach;?>
			<?if(isset($data['KILL_STATS'][$worldID])):?>
				<tr>
					<td colspan="2"><center>СТАТИСТИКА УБИЙСТВ</center></td>
				</tr>
				<?foreach($data['KILL_STATS'][$worldID] as $statName=>$statData):?>
					<tr>
						<td class='text-left'>
							<?=$statName?>&nbsp;&nbsp;
						</td>
						<td class='text-left'>
							<?=$statData?>
						</td>
					</tr>	
				<?endforeach;?>
			<?endif;?>
			<?if(isset($data['DEATH_STATS'][$worldID])):?>
				<tr>
					<td colspan="2"><center>СТАТИСТИКА СМЕРТЕЙ</center></td>
				</tr>			
				<?foreach($data['DEATH_STATS'][$worldID] as $statName=>$statData):?>
					<tr>
						<td class='text-left'>
							<?=$statName?>&nbsp;&nbsp;
						</td>
						<td class='text-left'>
							<?=$statData?>
						</td>
					</tr>	
				<?endforeach;?>
			<?endif;?>
		</tbody>
		</table>
	</div>
<?endforeach;?>