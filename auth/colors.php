<?	
	if(isset($_GET['del']))
	{
		$user->deleteEnderColor($_GET['del']);
		unset($_GET['del']);
		//echo "<meta http-equiv='refresh' content='0' url='index.php?p=invites'>";	
	}
	if(isset($_POST['eLetter1']) && isset($_POST['eLetter2']) && isset($_POST['eLetter3']))
	{
			$eCode = $_POST['eLetter1'].$_POST['eLetter2'].$_POST['eLetter3'];
			$user->claimEnderColor($eCode);
			echo "<meta http-equiv='refresh' content='0' url='index.php?p=colors'>";
	}
	$eColorsData = $user->getEnderColorsList('ALL');
?>
<style>
	<?foreach($eColors as $colorCode=>$colorInfo):?>
		.eCode<?=$colorCode?> {
			background-color: #<?=$colorInfo['RGB']?>; 
		}
	<?endforeach;?>
</style>
<h4>Ender-цвета</h4>
Не знаешь, что это? 
	<details>
	   <summary>Кликни сюда</summary>
	   <p>
		     В игре ты сможешь скрафтить особенный эндер-сундук и связать его с эндер-рюкзаком. 
		</br>Чтобы сделать это, ты должен покрасить оба предмета одинаковой комбинацией из трех цветов. 
		</br>Эту комбинацию я и решил назвать Ender-цветом для простоты. 
	   </p>
	</details>
</br>

<h4>Занятые Ender-цвета</h4>
<?foreach($eColorsData as $eColorData):?>
	<?showColor($eColorData['CODE'])?>
	<?if($eColorData['OWNER_ID']!=0):?> 
		[<?=$user->getUsernameOrID($eColorData['OWNER_ID'])?>]
	<?else:?>
		[СЕРВЕР]
	<?endif;?>
	<?if($user->isAdmin() | ($eColorData['OWNER_ID'] == $_SESSION['ID'])):?> 
		<a style='color: red; text-decoration: none;' href='index.php?p=colors&del=<?=$eColorData['CODE']?>'>[X]</a>
	<?endif;?>
	</br>
<?endforeach;?>
</br>
<form action='index.php?p=colors' method='POST'>
	<select style="height: 30px; width: 30px;" class="eCode0" name="eLetter1" required>
		<?foreach($eColors as $colorCode=>$colorInfo):?>
			<option value="<?=$colorCode?>" class="eCode<?=$colorCode?>" <?if($colorCode == '0'):?>selected<?endif;?>>&nbsp;&nbsp;&nbsp;&nbsp;</option>
		<?endforeach;?>
	</select>
	<select style="height: 30px; width: 30px; margin-left: -5px;" class="eCode0" name="eLetter2" required>
		<?foreach($eColors as $colorCode=>$colorInfo):?>
			<option value="<?=$colorCode?>" class="eCode<?=$colorCode?>" <?if($colorCode == '0'):?>selected<?endif;?>>&nbsp;&nbsp;&nbsp;&nbsp;</option>
		<?endforeach;?>
	</select>
	<select style="height: 30px; width: 30px; margin-left: -5px;" class="eCode0" name="eLetter3" required>
		<?foreach($eColors as $colorCode=>$colorInfo):?>
			<option value="<?=$colorCode?>" class="eCode<?=$colorCode?>" <?if($colorCode == '0'):?>selected<?endif;?>>&nbsp;&nbsp;&nbsp;&nbsp;</option>
		<?endforeach;?>
	</select>
	<input name="submitColor" type="submit" value="Занять цвет">
</form>

<script type="text/javascript">
 $("select").change(function() {
   $(this).removeClass($(this).attr('class'))
     .addClass($(":selected", this).attr('class'));
 });
</script>
