<?
	$codeData = $user->getInviteCodesList();
	$eColorsData = $user->getEnderColorsList();
?>
<h4>Добро пожаловать, <?=$user->getUsernameOrID($_SESSION['ID']);?>!</h4>
<?if($codeData != NULL):?>
</br>
<h4>Твои инвайт-коды</h4>
	<table>
		<?foreach($codeData as $codeInfo):?>
			<tr>
				<td style="color: lightgreen; line-height: 1.3;"><?=$codeInfo['CODE']?></td>
			</tr>
		<?endforeach;?>
	</table>
<?endif;?>
</br>
<h4>Твои Ender-цвета</h4>
<?if($eColorsData != NULL):?>
	<?foreach($eColorsData as $eColorData):?>
		<?showColor($eColorData['CODE'])?></br>
	<?endforeach;?>
<?else:?>
	<span style="color:	#FFCBDB;">
		Ты не занял себе ни один Ender-цвет.
		</br>Скорее сделай это на странице "<?=$menuPages['colors']['name']?>".</br>
	</span>
<?endif;?>
</br>
<h4>Связь с нами</h4>
Наша беседа <span style="color:rgb(74,118,168);">Вконтакте</span>: <a href='https://vk.me/join/AJQ1d6sS1wV3WEXlB4VTGVxx' target="_blank">ЗДЕСЬ</a> 
</br> Наш <span style="color:rgb(114,137,218);">Discord</span>-сервер: <a href='https://discordapp.com/invite/7TjEBeW' target="_blank">ЗДЕСЬ</a>
