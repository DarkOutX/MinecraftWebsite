<?
	session_start();
	require('classes/user.php');
	$user=new user();
	
	$menuPages = array(
		'profile' => array('name' => 'Профиль',    'needAdmin'=>false),
		'map'	  => array('name' => 'Карта',	   'needAdmin'=>false),
		'stats'   => array('name' => 'Статистика', 'needAdmin'=>false),
		'players' => array('name' => 'Игроки',     'needAdmin'=>false),
		'colors'  => array('name' => 'Ender-цвета','needAdmin'=>false),
		'invites' => array('name' => 'Инвайты',    'needAdmin'=>true ),
		'options' => array('name' => 'Настройки',  'needAdmin'=>false),
	);
	
	$eColors = array(
		'0' => array('nameRU' => 'белый', 'nameEN' => 'white',   'RGB' => 'E9ECEC'),
		'1'	=> array('nameRU' => 'оранжевый', 'nameEN' => 'orange',  'RGB' => 'F07613'),
		'2'	=> array('nameRU' => 'сиреневый', 'nameEN' => 'magenta',  'RGB' => 'BD44B3'),
		'3'	=> array('nameRU' => 'светло-синий', 'nameEN' => 'blue',  'RGB' => '3AAFD9'),
		'4'	=> array('nameRU' => 'жёлтый', 'nameEN' => 'yellow',  'RGB' => 'F8C627'),
		'5'	=> array('nameRU' => 'лаймовый', 'nameEN' => 'lime',  'RGB' => '70B919'),
		'6'	=> array('nameRU' => 'розовый', 'nameEN' => 'pink',  'RGB' => 'ED8DAC'),
		'7'	=> array('nameRU' => 'серый', 'nameEN' => 'gray',  'RGB' => '3E4447'),
		'8'	=> array('nameRU' => 'светло-серый', 'nameEN' => 'silver',  'RGB' => '8E8E86'),
		'9'	=> array('nameRU' => 'бирюзовый', 'nameEN' => 'cyan',  'RGB' => '158991'),
		'A'	=> array('nameRU' => 'фиолетовый', 'nameEN' => 'purple',  'RGB' => '792AAC'),
		'B'	=> array('nameRU' => 'синий', 'nameEN' => 'blue',  'RGB' => '35399D'),
		'C'	=> array('nameRU' => 'коричневый', 'nameEN' => 'brown',  'RGB' => '724728'),
		'D'	=> array('nameRU' => 'зелёный', 'nameEN' => 'green',  'RGB' => '546D1B'),
		'E'	=> array('nameRU' => 'красный', 'nameEN' => 'red',  'RGB' => 'A12722'),
		'F'	=> array('nameRU' => 'чёрный', 'nameEN' => 'black',  'RGB' => '141519')
	);		
	
	function showColor($eCode)
	{
		
	$eColors = array(
		'0' => array('nameRU' => 'белый', 'nameEN' => 'white',   'RGB' => 'E9ECEC'),
		'1'	=> array('nameRU' => 'оранжевый', 'nameEN' => 'orange',  'RGB' => 'F07613'),
		'2'	=> array('nameRU' => 'сиреневый', 'nameEN' => 'magenta',  'RGB' => 'BD44B3'),
		'3'	=> array('nameRU' => 'светло-синий', 'nameEN' => 'blue',  'RGB' => '3AAFD9'),
		'4'	=> array('nameRU' => 'жёлтый', 'nameEN' => 'yellow',  'RGB' => 'F8C627'),
		'5'	=> array('nameRU' => 'лаймовый', 'nameEN' => 'lime',  'RGB' => '70B919'),
		'6'	=> array('nameRU' => 'розовый', 'nameEN' => 'pink',  'RGB' => 'ED8DAC'),
		'7'	=> array('nameRU' => 'серый', 'nameEN' => 'gray',  'RGB' => '3E4447'),
		'8'	=> array('nameRU' => 'светло-серый', 'nameEN' => 'silver',  'RGB' => '8E8E86'),
		'9'	=> array('nameRU' => 'бирюзовый', 'nameEN' => 'cyan',  'RGB' => '158991'),
		'A'	=> array('nameRU' => 'фиолетовый', 'nameEN' => 'purple',  'RGB' => '792AAC'),
		'B'	=> array('nameRU' => 'синий', 'nameEN' => 'blue',  'RGB' => '35399D'),
		'C'	=> array('nameRU' => 'коричневый', 'nameEN' => 'brown',  'RGB' => '724728'),
		'D'	=> array('nameRU' => 'зелёный', 'nameEN' => 'green',  'RGB' => '546D1B'),
		'E'	=> array('nameRU' => 'красный', 'nameEN' => 'red',  'RGB' => 'A12722'),
		'F'	=> array('nameRU' => 'чёрный', 'nameEN' => 'black',  'RGB' => '141519')
	);		
		echo "<abbr title='[".$eColors[$eCode[0]]['nameRU'].", ".$eColors[$eCode[1]]['nameRU'].", ".$eColors[$eCode[2]]['nameRU']."]\n&nbsp;&nbsp;&nbsp;[".$eColors[$eCode[0]]['nameEN'].", ".$eColors[$eCode[1]]['nameEN'].", ".$eColors[$eCode[2]]['nameEN']."]'>";
		echo "<img class='eCodeWool' src='../assets/imgs/wool/".$eCode[0].".png'/>";
		echo "<img class='eCodeWool' src='../assets/imgs/wool/".$eCode[1].".png'/>";
		echo "<img class='eCodeWool' src='../assets/imgs/wool/".$eCode[2].".png'/>";
		echo "</abbr>";
	}
	
?>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Private Server</title>
		<script src="http://<?=$_SERVER['HTTP_HOST']?>/assets/js/jquery.min.js"></script>
		<!-- Bootstrap -->
		<script src="http://<?=$_SERVER['HTTP_HOST']?>/assets/js//bootstrap.min.js"></script>
		<link rel='stylesheet' href="http://<?=$_SERVER['HTTP_HOST']?>/assets/css/bootstrap.min.css">
		<link rel='stylesheet' href="http://<?=$_SERVER['HTTP_HOST']?>/assets/css/styles.css">
		<style>
			.eCodeWool{
				width: 28px;
				margin-bottom: 4px;
			}
			abbr {
				 text-decoration: none;
			}
			select::-ms-expand {
				display: none; 
			}
			select{
				-webkit-appearance: none;
				appearance: none;
			}
			@-moz-document url-prefix(){
				.ui-select{border: 1px solid #CCC; border-radius: 4px; box-sizing: border-box; position: relative; overflow: hidden;}
				.ui-select select { width: 110%; background-position: right 30px center !important; border: none !important;}
			}
			
			.stats-buttons{
				position: fixed;
				vertical-align: middle;
				width: 3.4%;
			}
			.stats-buttons a{
				vertical-align: middle;
				display: block;
				text-decoration: none;
				margin-left: 0;
				border-width: 3px 3px 3px 0;
			    border-style: solid;
				//border-color: rgb(91,63,44);
				border-color: rgb(80,80,80);
				border-radius: 0 10px 10px 0;
				color: brown;
				background-image: url(../assets/imgs/stone.png);
				height: 65px;
				margin-top: 8px;
				padding-left: 4px;
			}
			.stats-buttons a:hover{
				color: red;
			}
			
		</style>
		<?
			if($user->isAuthorized())
			{
				if($_GET['p'] == 'logout') $user->logout();				
			}
			else
			{
			    if(isset($_POST['inviteCode']))
				{
					$arFormResult = $user->register($_POST['username'],$_POST['password'],$_POST['inviteCode']);
					if($arFormResult['RESULT'] === true)
					{
						$formResult = $arFormResult['MESSAGE'];
					}
					else
					{
						$formResult = '';
						foreach($arFormResult['MESSAGE'] as $error) $formResult .= $error."</br>";
					}
				}
				elseif(isset($_POST['username']) && isset($_POST['password']))
				{
					if(!$user->login($_POST['username'],$_POST['password'])) $formResult = 'Указанные данные не верны.</br>';
				}
			}
		?>
	</head>
	<body <?if($_GET['p'] == 'stats'):?>onload='location.href="#total"'<?endif;?>>
		<a href="../" style="background-image:url('http://<?=$_SERVER['HTTP_HOST']?>/assets/imgs/titlehead.jpg');" class="titlehead">&nbsp;</a>
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <?if($user->isAuthorized()):?>
			  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <?endif;?>
			  <a class="navbar-brand" href="./"><span><?php echo $settings['general']['sitename']; ?></span></a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <?if($user->isAuthorized()):?>
				  <ul class="nav navbar-nav">
					<?foreach ($menuPages as $page=>$pageinfo):?>
						<?if(($pageinfo['needAdmin'] == $user->isAdmin()) | ($pageinfo['needAdmin'] != true)):?>
							<li><a href='index.php?p=<?=$page;?>'><?=$pageinfo['name']?></a></li>
						<?endif;?>
					<?endforeach;?>
				  </ul>
					<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="index.php?p=logout" class="dropdown-toggle"><span class="glyphicon glyphicon-user"></span>&nbsp;Выйти</a>
					</li>
				  </ul>
			  <?endif;?>
			</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		<?if($user->isAuthorized() && $_GET['p'] === 'map'):?>
			<?include('map.php');?>
		<?else:?>
		<div id="half" class="panel panel-default">
			<div class="panel-body">
				<?
					if($user->isAuthorized())
					{
						if(isset($_GET['p']))
						{
							$page = str_replace('/','',$_GET['p']);
							if(($pageinfo['needAdmin'] == $user->isAdmin()) | ($pageinfo['needAdmin'] != true) | ($page === 'id'))
							{
								include($page.'.php');
							}
						}
						else
						{
							include('./profile.php');
						}
					}
					else
					{
						if($_GET['p'] == 'register') include('./register.php'); else  include('./login.php');
						echo"</br><a href='../'>Вернуться на главную</a>";
					}
				?>
			</div>
		</div>
		<?endif;?>
	</body>
</html>
