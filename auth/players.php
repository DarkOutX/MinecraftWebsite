<?
if ($user->isAdmin())
{
	if(isset($_POST['action']) && isset($_POST['modifiedUserID']))
	{
		if 	   ($_POST['action'] == 'reset')  $user->changePassword('0000',$_POST['modifiedUserID']);
		elseif ($_POST['action'] == 'delete') $user->deleteUser($_POST['modifiedUserID']);
		echo "<meta http-equiv='refresh' content='0; url='index.php?p=players'>";
	}
}
$userData = $user->getUserList('ID','USERNAME','GROUP_ID','REFERRAL_ID'); //getting userInfo
?>
<style>
	a {text-decoration: none;}
</style>
<h4>Игроки сервера</h4>
<?foreach ($userData as $userInfo):?>
	<a  style="color:<?if($user->getUserOnlineStats($userInfo['USERNAME'],true)):?>green<?else:?>red<?endif;?>" href='index.php?p=stats&id=<?=$userInfo['USERNAME']?>'><?=$userInfo['USERNAME']?></a></br>
<?endforeach;?>
</br><span style='color:green;'>Игрок онлайн</span>
</br><span style='color:red;'>Игрок оффлайн</span>
</br><span style='font-size: 90%; color:rgb(150,150,150);'>*Нажми на никнейм, чтобы посмотреть статистику</span>

<?if($user->isAdmin()):?>
	</br>
	</br>
	<h4>Управление пользователями</h4>
	<form id="userModifyForm" action="index.php?p=players" method="post">
		<select style="height: 30px; width: 250px;" size="1" name="modifiedUserID" required>
			<option selected value='' disabled>Выберите игрока</option>
			<?foreach($userData as $userInfo):?>
				<option value='<?=$userInfo['ID']?>'><?=$userInfo['USERNAME']?></option>";
			<?endforeach;?>
		</select>
		<select style="height: 30px; width: 250px;" size="1" name="action" required>
			<option selected value='' disabled>Выберите действие</option>
			<option value='reset'>Пароль: 0000</option>
			<option value='delete'>Удалить из базы</option>
		</select>
		<input style="height: 30px; width: 250px;" type="submit" value="Подтвердить" onClick="return confirm('Ты уверен?');">
	</form>
<?endif;?>