<?php
class User {

//                                                                           
// M\   /M    M    MM  MM    MM      MMMMM   MM      MMMMMMM  MMMMMM  MM  MM 
// MM\ /MM   MMM   __  MMMM  MM      MM  MM  MM      MM   MM  MM      MM MM  
// MM\V/MM  MM MM  MM  MM MM MM      MMMMM   MM      MM   MM  MM      MMMM   
// MM V MM  MmmmM  MM  MM  MMMM      MM  MM  MM      MM   MM  MM      MM MM  
// MM   MM  M   M  MM  MM    MM      MMMMM   MMMMMM  MMMMMMM  MMMMMM  MM  MM 
//                                                                           
	
	//[table]: accounts
	//ID
	//HASH
	//GROUP_ID
	//USERNAME
	//PASSWORD
	//REFERRAL_ID
	
	//[table]: 	invite_codes
	//CODE
	//OWNER_ID
	
	//[table]: mysql_whitelist
	//user
	//UUID
	
	private $id;
	private $username;
	private $password;
	private $groupID;
	private $referralID;
	
	private $inviteCode;
	
	private $mysqlconnect;
	
	public function __construct()
    {   
        $this->mysqlconnect=new mysqli('localhost', 'root', 'password','minecraft');
        if ($this->mysqlconnect->connect_errno)
            die("Connection to database failed");
    }
	
//                                                                             
//   M    MM   MM  MMMMMM  MM  MM      MMMMM   MM      MMMMMMM  MMMMMM  MM  MM 
//  MMM   MM   MM  ""MM""  MM  MM      MM  MM  MM      MM   MM  MM      MM MM  
// MM MM  MM   MM    MM    MMMMMM      MMMMM   MM      MM   MM  MM      MMMM   
// MmmmM  MMM MMM    MM    MM  MM      MM  MM  MM      MM   MM  MM      MM MM  
// M   M   MMMMM     MM    MM  MM      MMMMM   MMMMMM  MMMMMMM  MMMMMM  MM  MM 
//                                                                              
          
	public function login($username, $password)
	{
		$this->username = $this->mysqlconnect->real_escape_string($username);
		$this->password = $password;
		// Вытаскиваем из БД запись, у которой логин равняеться введенному
		$data = mysqli_fetch_array($this->mysqlconnect->query("SELECT ID, GROUP_ID,PASSWORD FROM accounts WHERE USERNAME='".$this->mysqlconnect->real_escape_string($this->username)."' LIMIT 1"));
		// Сравниваем пароли
		if($data['PASSWORD'] === md5(md5($this->password)))
		{
			// Генерируем случайное число и шифруем его
			$hash = md5($this->generateCode(10));
			// Записываем в БД новый хеш авторизации
			$this->mysqlconnect->query("UPDATE accounts SET HASH='".$hash."' WHERE ID='".$data['ID']."'");

			$_SESSION['isAuthorized'] = 1;
			$_SESSION['ID'] = $data['ID'];
			$_SESSION['group'] = $data['GROUP_ID'];
			
			return true; //вместо header("Location: profile.php"); exit(); 
		}
		else
		{
			return false;
		}
	}
	
	public function logout()
	{
		session_unset();
		session_destroy();
		unset($_COOKIE);
	}
	
	public function register($username, $password, $inviteCode)
	{
		$err = [];
		$this->username = $this->mysqlconnect->real_escape_string($username);
		$this->password = $this->mysqlconnect->real_escape_string($password);
		$this->inviteCode = trim($this->mysqlconnect->real_escape_string($inviteCode));

		// проверям логин
		if(!preg_match("/^[a-zA-Z0-9]+$/",$this->username))
		{
			$err[] = "Логин может состоять только из букв английского алфавита и цифр";
		}

		if(strlen($this->username) < 3 or strlen($this->username) > 30)
		{
			$err[] = "Логин должен быть не меньше 3-х символов и не больше 30";
		}

		// проверяем, не сущестует ли пользователя с таким именем
		$query = $this->mysqlconnect->query("SELECT ID FROM accounts WHERE USERNAME='".$this->username."'");
		if(mysqli_num_rows($query) > 0)
		{
			$err[] = "Пользователь с таким логином уже существует в базе данных";
		}

		$query = mysqli_fetch_array($this->mysqlconnect->query("SELECT CODE FROM invite_codes WHERE CODE='".$this->inviteCode."'"));
		if ($query['CODE'] != $this->inviteCode)
		{
			$err[] = "Введенный инвайт-код не действителен";
		}
		// Если нет ошибок, то добавляем в БД нового пользователя
		if(count($err) == 0)
		{
			// Убираем лишние пробелы и делаем двойное хеширование
			$passwordHash = md5(md5(trim($this->password)));
			$referralData = mysqli_fetch_array($this->mysqlconnect->query("SELECT OWNER_ID FROM invite_codes WHERE CODE='".$this->inviteCode."'"));
			
			$this->mysqlconnect->query("INSERT INTO mysql_whitelist SET user='".$this->username."', UUID='".$this->getUUID($this->username)."'");
			$this->mysqlconnect->query("INSERT INTO accounts SET USERNAME='".$this->username."', PASSWORD='".$passwordHash."', REFERRAL_ID='".$referralData['OWNER_ID']."'");
			$this->mysqlconnect->query("DELETE FROM invite_codes WHERE CODE='".$this->inviteCode."'");
			$result['RESULT'] = true;
			$result['MESSAGE'] = 'Регистрация прошла успешно';
		}
		else
		{
            $result['RESULT'] = false;
            $result['MESSAGE'] = $err;
        }
        return $result;
    }
		
	public function changePassword($password, $id = NULL)
	{
		$password = $this->mysqlconnect->real_escape_string($password);
		if(isset($id))
		{
			$id = intval($id);
		}
		else
		{
			$id = intval($_SESSION['ID']);
		}
		$passwordHash = md5(md5($password));
		$this->mysqlconnect->query("UPDATE accounts SET accounts.PASSWORD='".$passwordHash."' WHERE ID=".$id);
	}
		
	public function createInviteCode($id)
	{
		if (strlen($id) <= 2)
		{
			$id = intval($id);
		}
		else
		{
			$id = $this->getUsernameOrID($id);
		}
		$inviteCode = $this->generateCode();
		$this->mysqlconnect->query("INSERT INTO invite_codes (CODE,OWNER_ID) VALUES('".$inviteCode."','".$id."');");
	}

//                                                                                                         
//  MMMMM   MMMMMM  MMMMMM      MM  MM    MM  MMMMMM  MMMMMMM      MMMMM   MM      MMMMMMM  MMMMMM  MM  MM 
// MM       MM      ""MM""      __  MMMM  MM  MM      MM   MM      MM  MM  MM      MM   MM  MM      MM MM  
// MM  MM   MMMMMM    MM        MM  MM MM MM  MMMMMM  MM   MM      MMMMM   MM      MM   MM  MM      MMMM   
// MM   MM  MM        MM        MM  MM  MMMM  MM      MM   MM      MM  MM  MM      MM   MM  MM      MM MM  
//  MMMMM   MMMMMM    MM        MM  MM    MM  MM      MMMMMMM      MMMMM   MMMMMM  MMMMMMM  MMMMMM  MM  MM 
//                                                                                                         

	private function generateCode($length=6) 
	{
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
		$code = "";
		$clen = strlen($chars) - 1;
		while (strlen($code) < $length) {
				$code .= $chars[mt_rand(0,$clen)];
		}
		return $code;
	}
	
	private function getUUID($string)
	{
		$string="OfflinePlayer:".$string;
		$val = md5($string, true);
		$byte = array_values(unpack('C16', $val));
	 
		$tLo = ($byte[0] << 24) | ($byte[1] << 16) | ($byte[2] << 8) | $byte[3];
		$tMi = ($byte[4] << 8) | $byte[5];
		$tHi = ($byte[6] << 8) | $byte[7];
		$csLo = $byte[9];
		$csHi = $byte[8] & 0x3f | (1 << 7);
	 
		if (pack('L', 0x6162797A) == pack('N', 0x6162797A)) {
			$tLo = (($tLo & 0x000000ff) << 24) | (($tLo & 0x0000ff00) << 8) | (($tLo & 0x00ff0000) >> 8) | (($tLo & 0xff000000) >> 24);
			$tMi = (($tMi & 0x00ff) << 8) | (($tMi & 0xff00) >> 8);
			$tHi = (($tHi & 0x00ff) << 8) | (($tHi & 0xff00) >> 8);
		}
	 
		$tHi &= 0x0fff;
		$tHi |= (3 << 12);
	   
		$uuid = sprintf(
			'%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x',
			$tLo, $tMi, $tHi, $csHi, $csLo,
			$byte[10], $byte[11], $byte[12], $byte[13], $byte[14], $byte[15]
		);
		return $uuid;
	}	
	
	public function getUsernameOrID($user)
	{
		if (strlen($user) < 3)
		{
			$data = mysqli_fetch_array($this->mysqlconnect->query("SELECT USERNAME FROM accounts WHERE ID='".$user."' LIMIT 1"));
			return $data['USERNAME'];
		}
		else
		{
			$data = mysqli_fetch_array($this->mysqlconnect->query("SELECT ID FROM accounts WHERE USERNAME='".$user."' LIMIT 1"));
			return $data['ID'];
		}
	}
	
	public function getUserList($var1 = NULL, $var2 = NULL, $var3 = NULL, $var4 = NULL)
	{
		if (isset($var1) && isset($var2) && isset($var3) && isset($var4)) 
		{
			$userDataMYSQL = $this->mysqlconnect->query("SELECT ".$var1.", ".$var2.", ".$var3.", ".$var4."  FROM accounts");
		}
		elseif(isset($var1) && isset($var2) && isset($var3))
		{
			$userDataMYSQL = $this->mysqlconnect->query("SELECT ".$var1.",".$var2.",".$var3."  FROM accounts");
		}
		elseif(isset($var1) && isset($var2))
		{
			$userDataMYSQL = $this->mysqlconnect->query("SELECT ".$var1.",".$var2." FROM accounts");
		}
		elseif(isset($var1))
		{
			$userDataMYSQL = $this->mysqlconnect->query("SELECT ".$var1."  FROM accounts");
		}
		else
		{
			$userDataMYSQL = $this->mysqlconnect->query("SELECT * FROM accounts");
		}
		if (mysqli_num_rows($userDataMYSQL)) 
		{
			$userData = array();
			while($userInfo = mysqli_fetch_array($userDataMYSQL)) 
			{
				$userData[] = $userInfo;
			}
			return $userData;
		}
		else
		{
			return NULL;
		}
	}
	
	public function getInviteCodesList($isAdmin = false)
	{
		if($isAdmin)
		{
			$codesDataMYSQL = $this->mysqlconnect->query("SELECT *  FROM invite_codes");
		}
		else
		{
			$codesDataMYSQL = $this->mysqlconnect->query("SELECT *  FROM invite_codes WHERE OWNER_ID=".$_SESSION['ID']);
		}
		
		if (mysqli_num_rows($codesDataMYSQL)) 
		{
			$inviteCodesData = array();
			while($inviteCodeInfo = mysqli_fetch_array($codesDataMYSQL)) 
			{
				$inviteCodesData[] = $inviteCodeInfo;
			}
			return $inviteCodesData;
		}
		else
		{
			return NULL;
		}
	}
	
	public function isAuthorized()
	{
		return ($_SESSION['isAuthorized'] == 1) ?  true : false;
	}
	
	public function isAdmin()
	{
		return ($_SESSION['group'] == 1) ?  true : false;
	}

//                                                                                                         
// MMMMM   MMMMMM  MM      MMMMMM  MMMMMM  MMMMMM     MM  MM    MM  MMMMMM  MMMMMMM      MMMMM   MM      MMMMMMM  MMMMMM  MM  MM 
// MM  MM  MM      MM      MM      ""MM""  MM         __  MMMM  MM  MM      MM   MM      MM  MM  MM      MM   MM  MM      MM MM  
// MM   M  MMMMMM  MM      MMMMMM    MM    MMMMMM     MM  MM MM MM  MMMMMM  MM   MM      MMMMM   MM      MM   MM  MM      MMMM   
// MM  MM  MM      MM      MM        MM    MM         MM  MM  MMMM  MM      MM   MM      MM  MM  MM      MM   MM  MM      MM MM  
// MMMMM   MMMMMM  MMMMMM  MMMMMM    MM    MMMMMM     MM  MM    MM  MM      MMMMMMM      MMMMM   MMMMMM  MMMMMMM  MMMMMM  MM  MM 
//  	
		
	public function deleteUser($id)
	{
		if ($this->isAdmin())
		{
			if (strlen($id) > 3)
			{
				$username = $this->getUsernameOrID($username);
			}
			else
			{
				$username = $this->mysqlconnect->real_escape_string($username);
			}
			$this->mysqlconnect->query("DELETE FROM invite_codes WHERE OWNER_ID='".$id."'");
			$this->mysqlconnect->query("DELETE FROM ender_colors WHERE OWNER_ID='".$id."'");
			$this->mysqlconnect->query("DELETE FROM accounts WHERE ID='".$id."'");
			$this->mysqlconnect->query("DELETE FROM mysql_whitelist WHERE user='".$this->getUsernameOrID($id)."'");
			$this->mysqlconnect->query("DELETE FROM dynmap_faces WHERE PlayerName='".$this->getUsernameOrID($id)."'");
			$this->deleteStats($id);
		}
	}
	
	function deleteStats($id)
	{
		$statsID = mysqli_fetch_array($this->mysqlconnect->query("SELECT player_id FROM Stats_players WHERE name='".$this->getUsernameOrID($id)."' LIMIT 1"));
		$this->mysqlconnect->query("DELETE FROM Stats_block WHERE player_id='".$statsID."'");
		$this->mysqlconnect->query("DELETE FROM Stats_death WHERE player_id='".$statsID."'");
		$this->mysqlconnect->query("DELETE FROM Stats_kill WHERE player_id='".$statsID."'");
		$this->mysqlconnect->query("DELETE FROM Stats_move WHERE player_id='".$statsID."'");
		$this->mysqlconnect->query("DELETE FROM Stats_player WHERE player_id='".$statsID."'");
		$this->mysqlconnect->query("DELETE FROM Stats_pvp WHERE player_id='".$statsID."'");
		$this->mysqlconnect->query("DELETE FROM Stats_players WHERE player_id='".$statsID."'");	
	}
	
	public function deleteInviteCode($code)
	{
		$this->mysqlconnect->query("DELETE FROM invite_codes WHERE CODE='".$code."'");
	}
	
	public function deleteEnderColor($eCode)
	{
		if ($this->isAdmin())
		{
			$this->mysqlconnect->query("DELETE FROM ender_colors WHERE CODE='".$eCode."'");
		}
		else
		{
			$eColorData = mysqli_fetch_array($this->mysqlconnect->query("SELECT *  FROM ender_colors WHERE CODE=".$eCode));
			if ($eColorData['OWNER_ID'] == $_SESSION['ID']) $this->mysqlconnect->query("DELETE FROM ender_colors WHERE CODE='".$eCode."'");
		}
	}

//                                                                                                         
//  MMMM  MMMMMM    M    MMMMMM   MMMM       MMMMM   MM      MMMMMMM  MMMMMM  MM  MM 
// MM     ""MM""   MMM   ""MM""  MM          MM  MM  MM      MM   MM  MM      MM MM  
//  MMMM    MM    MM MM    MM     MMMM       MMMMM   MM      MM   MM  MM      MMMM   
//     MM   MM    MmmmM    MM        MM      MM  MM  MM      MM   MM  MM      MM MM  
//  MMMM    MM    M   M    MM     MMMM       MMMMM   MMMMMM  MMMMMMM  MMMMMM  MM  MM 
//   	
	
public function getUserStats($username)
{
	if (strlen($username) < 3)
	{
		$username = $this->getUsernameOrID($username);
	}
	else
	{
		$username = $this->mysqlconnect->real_escape_string($username);
	}
	$tempData = mysqli_fetch_array($this->mysqlconnect->query("SELECT player_id FROM Stats_players WHERE name='".$username."' LIMIT 1"));
	$usernameID = $tempData['player_id'];
	
	$tempData = $this->mysqlconnect->query("SELECT * FROM Stats_player WHERE player_id='".$usernameID."'");
	if ($tempData->num_rows > 0) 
	{   
		while($arr = $tempData->fetch_assoc()) 
		{
			if(isset($arr["world"]))
			{
				$arStats['MAIN_STATS'][$arr["world"]] = $arr;
			}
		}
	}
	$tempData = $this->mysqlconnect->query("SELECT * FROM Stats_move WHERE player_id='".$usernameID."'");
	if ($tempData->num_rows > 0) 
	{   
		while($arr = $tempData->fetch_assoc()) 
		{
			if(isset($arr["world"]))
			{
				$arStats['MOVE_STATS'][$arr["world"]][$arr["type"]] = $arr["distance"];
			}
		}
	}
	$tempData = $this->mysqlconnect->query("SELECT * FROM Stats_kill WHERE player_id='".$usernameID."'");
	if ($tempData->num_rows > 0) 
	{   
		while($arr = $tempData->fetch_assoc()) 
		{
			if(isset($arr["world"]))
			{
				$arStats['KILL_STATS'][$arr["world"]][$arr["type"]] = $arr["amount"];
			}
		}
	}
	$tempData = $this->mysqlconnect->query("SELECT * FROM Stats_death WHERE player_id='".$usernameID."'");
	if ($tempData->num_rows > 0) 
	{   
		while($arr = $tempData->fetch_assoc()) 
		{
			if(isset($arr["world"]))
			{
				$arStats['DEATH_STATS'][$arr["world"]][$arr["cause"]] = $arr["amount"];
			}
		}
	}
	
	//generating TOTAL
	foreach($arStats as $statGroupName=>$statGroupData)
	{
		foreach($statGroupData as $statNameTemp)
		{
			foreach($statNameTemp as $statName=>$statValue)
			{
				$arStats[$statGroupName]['total'][$statName] += $statValue;
			}
		}
	}
	//forming time strings
	
	
	//$tempData = mysqli_fetch_array($this->mysqlconnect->query("SELECT * FROM Stats_block WHERE player_id='".$usernameID."'"));
	//$arStats['BLOCK_STATS'] = $tempData;
	//$tempData = mysqli_fetch_array($this->mysqlconnect->query("SELECT * FROM Stats_pvp WHERE player_id='".$usernameID."'"));
	//$arStats['PVP_STATS'] = $tempData;
	if(isset($arStats))	return $arStats;
	               else return array(); 
}	

public function getUserOnlineStats($username, $statusOnly = false)
{
	if (strlen($username) < 3)
	{
		$username = $this->getUsernameOrID($username);
	}
	else
	{
		$username = $this->mysqlconnect->real_escape_string($username);
	}
	$tempData = mysqli_fetch_array($this->mysqlconnect->query("SELECT player_id FROM Stats_players WHERE name='".$username."' LIMIT 1"));
	$arStats['USERNAME'] = $username;
	$arStats['USER_STATS_ID'] = $tempData['player_id'];
	
	$tempData = mysqli_fetch_array($this->mysqlconnect->query("SELECT lastleave,lastjoin FROM Stats_player WHERE player_id='".$arStats['USER_STATS_ID']."' LIMIT 1"));
	$arStats['lastleave'] = $tempData['lastleave'];
	$arStats['lastjoin'] = $tempData['lastjoin'];
	
	$leavetime = strtotime($tempData['lastleave']);
	$jointime = strtotime($tempData['lastjoin']);
	
	
	$sessLength = intval($leavetime - $jointime); // <last session in seconds>
	
	if ($sessLength >= 0)
	{
		$arStats['isOnline'] = false;
		if (!$statusOnly)
		{	
			// days: 86400, hours:3600, mins:60, secs: 1
			$arStats['lastSession']['length'] = ($sessLength % 60)."с"; 
			if($sessLength > 59) 
				$arStats['lastSession']['length'] = floor($sessLength %86400 %3600/ 60)."м ".$arStats['lastSession']['length']; 
			if($sessLength > 3599) 
				$arStats['lastSession']['length'] = floor($sessLength %86400 / 3600)."ч ".$arStats['lastSession']['length']; 
			if($sessLength > 86399) 
				$arStats['lastSession']['length'] = floor($sessLength / 86400)."д ".$arStats['lastSession']['length']; 
			
			$timeago = time() - $leavetime;
			if($timeago < 60)
				$arStats['lastSession']['ago'] = $timeago."с";
			elseif($timeago < 3600)
				$arStats['lastSession']['ago'] = floor($timeago/60)."м";
			elseif($timeago < 86400)
				$arStats['lastSession']['ago'] = floor($timeago/3660)."ч";
			else
				$arStats['lastSession']['ago'] = floor($timeago/86400)."д";			
		}
	}
	else
	{
		$arStats['isOnline'] = true;
	}
	
	if (!$statusOnly) return $arStats;
				else  return $arStats['isOnline'];
}	

//                                                                                                         
// MMMMMM  MMMMMM  M\   /M  MMMMM       MMMMM   MM      MMMMMMM  MMMMMM  MM  MM 
// ""MM""  MM      MM\ /MM  MM  MM      MM  MM  MM      MM   MM  MM      MM MM  
//   MM    MMMMMM  MM\V/MM  MMMMM       MMMMM   MM      MM   MM  MM      MMMM   
//   MM    MM      MM V MM  MM          MM  MM  MM      MM   MM  MM      MM MM  
//   MM    MMMMMM  MM   MM  MM          MMMMM   MMMMMM  MMMMMMM  MMMMMM  MM  MM 
//   
	
	private function setUserInfo($userID)
	{
		$this->id = intval($userID);
		$userData = mysqli_fetch_array($this->mysqlconnect->query("SELECT * FROM accounts WHERE ID = '".$this->id."' LIMIT 1"));
		$this->username = $userData['USERNAME'];
		$this->groupID = $userData['GROUP_ID'];
		$this->referralID = $userData['REFERRAL_ID'];
	}
	
	public function var_dump_extended($array)
	{
		echo "</br>";
		foreach($array as $key=>$subarray)
		{
			if(!is_array($subarray))
			{
				echo "// ".$key." </br>";
			}
			else
			{
				echo "// [".$key."] </br>";
				foreach($array[$key] as $subkey=>$subsubarray)
				{
					if(!is_array($subsubarray))
					{
						echo "//&nbsp;&nbsp ⤷".$subkey." </br>";
					}
					else
					{	
						echo "//&nbsp;&nbsp; [".$subkey."] </br>";
						foreach($array[$key][$subkey] as $subsubkey=>$subsubsubarray)
						{
							echo "//&nbsp;&nbsp;&nbsp;&nbsp; ⤷".$subsubkey." </br>";
						}
					}
				}
			}
		}
	}

	public function claimEnderColor($eCode,$id = NULL)
	{
		$eCode = $this->mysqlconnect->real_escape_string($eCode);
		if(isset($id))
		{
			$id = intval($id);
		}
		else
		{
			$id = intval($_SESSION['ID']);
		}
		$err = [];
		$query = $this->mysqlconnect->query("SELECT CODE FROM ender_colors WHERE CODE='".$eCode."'");
		if(mysqli_num_rows($query) > 0)
		{	
			$err[] = "Выбранный цвет уже занят.";
		}
		$query = $this->mysqlconnect->query("SELECT CODE FROM ender_colors WHERE OWNER_ID='".$id."'");
		if(mysqli_num_rows($query) >= 3)
		{	
			$err[] = "Вы уже заняли слишком много цветов.";
		}
		
		if(count($err) == 0)
		{
			$this->mysqlconnect->query("INSERT INTO ender_colors (CODE,OWNER_ID) VALUES('".$eCode."','".$id."');");
			return true;
		}
		else
		{
			return $err;
		}
	}
	
	public function getEnderColorsList($id = NULL)
	{
		if(isset($id) && $id != 'ALL')
		{
			$id = intval($id);
			if (strlen($id) >= 3)
			{
				$id = $this->getUsernameOrID($username);
			}
		}
		elseif(!isset($id))
		{
			$id = intval($_SESSION['ID']);
		}
		
		if($id == 'ALL')
		{
			$eColorsDataMYSQL = $this->mysqlconnect->query("SELECT *  FROM ender_colors ORDER BY OWNER_ID ASC");
		}
		else
		{
			$eColorsDataMYSQL = $this->mysqlconnect->query("SELECT *  FROM ender_colors WHERE OWNER_ID=".$id);
		}
		
		if (mysqli_num_rows($eColorsDataMYSQL)) 
		{
			$eColorsData = array();
			while($inviteCodeInfo = mysqli_fetch_array($eColorsDataMYSQL)) 
			{
				$eColorsData[] = $inviteCodeInfo;
			}
			return $eColorsData;
		}
		else
		{
			return NULL;
		}
	}
	
}	
?>