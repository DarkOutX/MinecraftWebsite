<?
class Ping 
{
	private function getPing() 
	{
		$settings = parse_ini_file('./db.ini',true);

		$ipandport = explode(':',$settings['mcserver']['ip']);
		if(empty($ipandport[1]))
		{
		$ipandport[1] = 25565;
		}

		// Edit this ->
		define( 'MQ_SERVER_ADDR', $ipandport[0]);
		define( 'MQ_SERVER_PORT', $ipandport[1]);
		define( 'MQ_TIMEOUT', 1 );
		// Edit this <-

		// Display everything in browser, because some people can't look in logs for errors
		Error_Reporting( E_ALL | E_STRICT );
		Ini_Set( 'display_errors', true );

		require __DIR__ . './minecraftPing.php';
	
		$Timer = MicroTime( true );

		$Info = false;
		$Query = null;

		try
		{
			$Query = new MinecraftPing( MQ_SERVER_ADDR, MQ_SERVER_PORT, MQ_TIMEOUT );

			$Info = $Query->Query( );

			if( $Info === false )
			{
				/*
				 * If this server is older than 1.7, we can try querying it again using older protocol
				 * This function returns data in a different format, you will have to manually map
				 * things yourself if you want to match 1.7's output
				 *
				 * If you know for sure that this server is using an older version,
				 * you then can directly call QueryOldPre17 and avoid Query() and then reconnection part
				 */

				$Query->Close( );
				$Query->Connect( );

				$Info = $Query->QueryOldPre17( );
			}
		}
		catch( MinecraftPingException $e )
		{
			$Exception = $e;
		}
		
		
		if( $Query !== null )
		{
			$Query->Close( );
		}

		$Timer = Number_Format( MicroTime( true ) - $Timer, 4, '.', '' );
		$Info['ping'] = $Timer;
		$Info['ip'] = $settings['mcserver']['ip'];

		if(!file_exists('./modlist.json'))
		{
			$mods = array();
			foreach ($Info['modinfo']["modList"] as $mod)
			{
				$mods[$mod['modid']] = $mod['version'];
			}
			file_put_contents('./modlist.json',json_encode($mods));  // Перекодировать в формат и записать в файл.    
		}
		
		if(isset($Info['players']))
		{
			return $Info;
		}
		else
		{
			return False;
		}
	}
	
	public function showPing() 
	{
		$pingInfo = $this->getPing();
		if ($pingInfo) {
			require './onlineMSG.php';	
		} else {
			require './offlineMSG.php';	
		}	
	}
}
?>