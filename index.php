<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <title>Private Server</title>

  <!-- meta -->
  <meta name="description" content="Minecraft Portal Website Template">
  <meta name="tags" content="minecraft, website, play, mc, template">

  <!-- css -->
  <link rel="stylesheet" type="text/css" href="assets/css/main/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="assets/css/main/style.css">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="shortcut icon" type="image/png" href="favicon.ico"/>

     <!-- javascript -->
      <script type="text/javascript" src="assets/js/jquery.min.js"></script>
      <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

      <script type="text/javascript">

      // Update the button indicator on mouse enter
      $('.icon').mouseenter(function() {
        $('.indicator').html(this.id);
      });

      // Clear the button indicator on mouse leave
      $('.icon').mouseleave(function() {
        $('.indicator').html('&nbsp;');
      });

      // Procedural fade of each element
      $(document).ready(function() {
        $('img').fadeIn(function() {
          $('h1').fadeIn(function() {
            $('p').fadeIn(function() {
              $('.icons').fadeIn(function() {
                $('.footer').fadeIn();
              });
            });
          });
        });
      });

      </script>

	<script>
		$(document).ready(function() {
		$("#pingBlock").load("./serverStatus.php");
		setInterval(update, 5000);
		});
		function update() {
		$("#pingBlock").load("./serverStatus.php");
		}
	</script>
	  
    </head>
    <body>

      <!-- banner start -->
      <div class="banner">

        <img style="display:none" class="img-responsive" src="http://img1.wikia.nocookie.net/__cb20111230161227/assassinscreed/images/7/70/Minecraft-logo.png" width="400px">
	    <h1 style="display:none;">
          Добро пожаловать!
        </h1>
		</br>
		<div id="pingBlock">Пингуем сервер...</div>
        <p style="display:none" class="btn btn-ip"><a href='./auth'>Личный кабинет</a></p></br>
        <p style="display:none" class="btn btn-ip"><a href='https://yadi.sk/d/jzD04QbJ3Y6EEx' target="_blank">Скачать всю сборку</a></p></br>
        <p style="display:none" class="btn btn-ip"><a href='https://yadi.sk/d/Zd35Ph7p3Y6EEn' target="_blank">Скачать лаунчер</a></p></br>
        <p style="display:none" class="btn btn-ip"><a href='https://yadi.sk/d/nowJe-F23Y6EEr' target="_blank">Скачать только моды и jar</a></p></br>
        <?
			/*
			$modlistFile = file_get_contents('modlist.json');
			$serverInfo = json_decode($modlistFile, true);
			foreach($serverInfo as $name=>$version) echo $name." (".$version.")</br>";
			*/
		?>	
      </div>
      <!-- banner end -->
      <!-- footer start -->
      <p class="footer" style="display:none">m7.zapto.org</p> 
      <!-- footer end -->


   
    </body>
    </html>